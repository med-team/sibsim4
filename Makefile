# $Id: Makefile,v 1.23 2008/03/30 22:58:17 c4chris Exp $
#
# Christian Iseli, LICR ITO, Christian.Iseli@licr.org
#
# Copyright (c) 2001-2005 Swiss Institute of Bioinformatics.
# Copyright (C) 1998-2001  Liliana Florea.


DEBUG =


# For better performance, replace ``-O'' with whatever
# the best optimization flag is for your computer.
# For Sun's compilers under Solaris, ``-fast'' works well.
# For gcc, ``-O2'' works well.

OPT = -O


# The default CFLAGS is meant for GCC.  If you compile for Solaris, you need
# to change it to this:
# CFLAGS = -Xc

CFLAGS = -std=gnu99 -W -Wall -Wconversion -pedantic $(DEBUG) $(OPT)


# The default is GCC.  On Solaris, you might put:
# CC = /opt/SUNWspro/bin/cc

CC = gcc


# Depending on the compile flags you use, you might need to explicitly use the
# math library:
# LIBS = -lm

LIBS = 


# There should be no need to change things below...

OBJS = sim4b1.o align.o misc.o sim4.init.o

sim4: $(OBJS)
	$(CC) -o SIBsim4 $(CFLAGS) $(OBJS) $(LIBS)

clean:
	rm -f SIBsim4 *.o

# DO NOT DELETE
